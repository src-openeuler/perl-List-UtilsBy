%global cpan_name List-UtilsBy
%global _empty_manifest_terminate_build 0

Name:           perl-%{cpan_name}
Version:        0.12
Release:        2
Summary:        Higher-order list utility functions

License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{cpan_name}/
Source0:        https://www.cpan.org/authors/id/P/PE/PEVANS/%{cpan_name}-%{version}.tar.gz 

BuildArch:      noarch

BuildRequires:  perl-generators
BuildRequires:  perl(Exporter) >= 5.57
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(Test::More) >= 0.88
Requires:       perl(Exporter) >= 5.57

%description
This module provides a number of list utility functions, all of which take
an initial code block to control their behaviour. They are variations on
similar core perl or List::Util functions of similar names, but which use
the block to control their behaviour. For example, the core Perl function
sort takes a list of values and returns them, sorted into order by their
string value. The "sort_by" function sorts them according to the string
value returned by the extra function, when given each value.

%package help
Summary : Higher-order list utility functions
Provides: perl-%{cpan_name}-doc

%description help
This module provides a number of list utility functions, all of which take
an initial code block to control their behaviour. They are variations on
similar core perl or List::Util functions of similar names, but which use
the block to control their behaviour. For example, the core Perl function
sort takes a list of values and returns them, sorted into order by their
string value. The "sort_by" function sorts them according to the string
value returned by the extra function, when given each value.

%prep
%setup -q -n %{cpan_name}-%{version}

%build
export PERL_MM_OPT=""
%{__perl} Build.PL --installdirs=vendor
./Build


%install
./Build install --destdir=$RPM_BUILD_ROOT --create_packlist=0

%{_fixperms} $RPM_BUILD_ROOT/*

%check
./Build test

%files
%doc Changes META.json README
%license LICENSE
%{perl_vendorlib}/*

%files help
%{_mandir}/man?/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.12-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Sep 08 2023 xu_ping <707078654@qq.com> 0.12-1
- Upgrade version to 0.12

* Wed Jul 14 2021 ice-kylin <wminid@yeah.net> 0.11-1
- Initial package
